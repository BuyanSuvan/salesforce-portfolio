import { LightningElement, api, wire, track } from "lwc";
import getPositionByStatus from "@salesforce/apex/PositionByStatusForLwcCtrl.getPositionByStatus";
import { getPicklistValues } from "lightning/uiObjectInfoApi";
import STATUS_FIELD from "@salesforce/schema/Position__c.Status__c";
const DEFAULT_FILTER = "All";
import getTotalRecordsCount from '@salesforce/apex/PositionByStatusForLwcCtrl.getTotalRecordsCount';

export default class tableWithComboBox extends LightningElement {
  @track positionsByStatus;
  @track statusFilter = DEFAULT_FILTER;
  @track pickListvalues;
  @track error;
  @track values;
  @track offset = 0;
  @track pagesize = 3;
  @track page = 1;
  @api totalrecords;
  @api totalpages;

  @wire(getPicklistValues, {
    recordTypeId: "012000000000000AAA",
    fieldApiName: STATUS_FIELD
  })
  wiredPickListValue({ data, error }) {
    if (data) {
      console.log(` Picklist values are `, data.values);
      this.pickListvalues = [{ label: DEFAULT_FILTER, value: DEFAULT_FILTER, selected: true },
      ...data.values];
      this.error = undefined;
    } if (error) {
      console.log(` Error while fetching Picklist values  ${error}`);
      this.error = error;
      this.pickListvalues = undefined;
    }
  }
  @wire(getPositionByStatus, { statusFilter: "$statusFilter", offset: "$offset", pageSize: "$pagesize" }) positionsByStatus;
  handleChange(event) {
    this.statusFilter = event.target.value;
  }
  @wire(getTotalRecordsCount, { statusFilter: "$statusFilter" })
  wiredTotalRecords({ error, data }) {
    if (data) {
      this.totalrecords = data;
    } else if (error) {
      this.error = error;
    }
  }
  get totalPages() {
    return Math.ceil(this.totalrecords / this.pagesize);
  }
  handlePrevious() {
    if (this.offset - this.pagesize >= 0) {
      this.offset = this.offset - this.pagesize;
      this.page = this.page - 1;
    }
  }
  handleNext() {
    if ((this.page < this.totalPages) && this.page !== this.totalPages) {
      this.offset = this.offset + this.pagesize;
      this.page = this.page + 1;
    }
  }
  @track pageList;
  connectedCallback() {
    this.pageList = [];
    this.generatePageList();
  }

  generatePageList() {
    this.pageList = [];

    if (this.totalPages > 1) {
      if (this.totalPages <= 10) {
        let counter = 2;
        for (; counter < (this.totalPages); counter++) {
          this.pageList.push(counter);
        }
      } else {
        if (this.page < 5) {
          this.pageList.push(2, 3);
        } else {
          if (this.page > (this.totalPages - 5)) {
            this.pageList.push('...', (this.totalPages - 5), (this.totalPages - 4), (this.totalPages - 3), (this.totalPages - 2), (this.totalPages - 1));
          } else {
            this.pageList.push('...', (this.page - 2), (this.page - 1), (this.page), (this.page + 1), (this.page + 2), '...');
          }
        }
      }
    }
  }
  hadleClickPage(event) {
    event.preventDefault()
    this.page = parseInt(event.target.name, 10);
    this.template.querySelectorAll("a.slds-p-horizontal_x-small").forEach(element => {
      element.classList.remove("slds-p-horizontal_x-small");
    });
    this.generatePageList();
    event.target.classList.add("slds-p-horizontal_x-small");
  }
}
