import { LightningElement, track } from "lwc";
export default class PaginationNavigation extends LightningElement {
  @track pageList;

  connectedCallback() {
    this.pageList = [];
    this.generatePageList();
  }
  hadleClickPage(event) {
    this.dispatchEvent(new CustomEvent("pageclick"));
  }
  handlePrevious(_event) {
    this.dispatchEvent(new CustomEvent("previous"));
  }
  handleNext(_event) {
    this.dispatchEvent(new CustomEvent("next"));
  }
}