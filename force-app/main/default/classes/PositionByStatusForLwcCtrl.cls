public with sharing class PositionByStatusForLwcCtrl {
  @AuraEnabled
  public final string DEFAULT_FILTER = 'All';

  @AuraEnabled(cacheable=true)
  public static List<Position__c> getPositionByStatus(
    Integer offset, Integer pageSize, String statusFilter ) {
    if (statusFilter == 'All') {
      return [
        SELECT
          Id,
          Name,
          Status__c,
          Begin_Date__c,
          Deadline__c,
          Salary_Max__c,
          Salary_Min__c
        FROM Position__c
        ORDER BY Name ASC
        LIMIT : pageSize
        OFFSET : offset
      ];
    } else {
      return [
        SELECT
          Id,
          Name,
          Status__c,
          Begin_Date__c,
          Deadline__c,
          Salary_Max__c,
          Salary_Min__c
        FROM Position__c
        WHERE Status__c = :statusFilter
        ORDER BY Name ASC
        LIMIT : pageSize
        OFFSET : offset
      ];
    }
  }
  @AuraEnabled(cacheable=true)
  public static Integer getTotalRecordsCount(String statusFilter) {
    if (statusFilter == 'All') {
      return [SELECT COUNT() FROM Position__c];
    } else {
      return [SELECT COUNT() FROM Position__c WHERE Status__c = :statusFilter];
    }
  }
 
}