public with sharing class PositionByStatusCtrl {
  public static final String DEFAULT_FILTER = 'All';
  public String statusFilter { get; set; }
  public List<Position__c> positionlist { get; set; }

  public PositionByStatusCtrl() {
    statusFilter = DEFAULT_FILTER;
    filterPositions();
  }

  public List<SelectOption> getstatusList() {
    List<SelectOption> options = new List<SelectOption>();
    Schema.DescribeFieldResult fieldResult = Position__c.Status__c.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    options.add(new SelectOption(DEFAULT_FILTER, DEFAULT_FILTER));
    for (Schema.PicklistEntry p : ple) {
      options.add(new SelectOption(p.getLabel(), p.getValue()));
    }
    return options;
  }

  public void filterPositions() {
    if (statusFilter == DEFAULT_FILTER) {
      positionlist = [
        SELECT
          Name,
          Status__c,
          Begin_Date__c,
          Deadline__c,
          Salary_Max__c,
          Salary_Min__c
        FROM Position__c
        ORDER BY Name ASC
      ];
    } else {
      positionlist = [
        SELECT
          Name,
          Status__c,
          Begin_Date__c,
          Deadline__c,
          Salary_Max__c,
          Salary_Min__c
        FROM Position__c
        WHERE Status__c = :statusFilter
        ORDER BY Name ASC
      ];
    }
  }
  public void save() {
    ApexPages.addMessage(
      new ApexPages.message(
        ApexPages.Severity.CONFIRM,
        'Saved very successfully!'
      )
    );
    update positionlist;
    statusFilter = DEFAULT_FILTER;
    filterPositions();
  }
}
