@isTest
public class PositionByStatusCtrlTest {
  @testSetUp
  static void setUpPositions() {
    Position__c forSetUp = new Position__c();
    forSetUp.Name = '.Net developer';
    forSetUp.Status__c = 'Open';
    insert forSetUp;

    Position__c forSetUp2 = new Position__c();
    forSetUp2.Name = 'Java developer';
    forSetUp2.Status__c = 'Open';
    insert forSetUp2;

    Position__c forSetUp3 = new Position__c();
    forSetUp3.Name = 'Python developer';
    forSetUp3.Status__c = 'Closed';
    insert forSetUp3;

    Position__c forSetUp4 = new Position__c();
    forSetUp4.Name = 'Account manager';
    forSetUp4.Status__c = 'Closed-Filled';
    insert forSetUp4;

    Position__c forSetUp5 = new Position__c();
    forSetUp5.Name = 'Java junior developer';
    forSetUp5.Status__c = 'Open';
    insert forSetUp5;
  }

  @isTest
  static void testPositionByStatusCtrl() {
    Test.startTest();
    PositionByStatusCtrl controller = new PositionByStatusCtrl();
    Test.stopTest();

    System.assertEquals(5, controller.positionList.Size());
    System.assertEquals('All', controller.statusFilter);
  }

  @isTest
  static void testSaveAndMessage() {
    PositionByStatusCtrl controllerForSave = new PositionByStatusCtrl();

    Test.startTest();
    controllerForSave.statusFilter = 'Open';
    controllerForSave.filterPositions();
    Id testId = controllerForSave.positionList.get(0).Id;
    controllerForSave.positionlist.get(0).Status__c = 'Closed';
    controllerForSave.save();
<<<<<<< HEAD
    Position__c actualStatusOfSelectedPosition = [
      SELECT Status__c
      FROM Position__c
      WHERE Id = :testId
    ];
    Test.stopTest();

=======
    Position__c actualStatusOfSelectedPosition = [SELECT Status__c FROM Position__c WHERE Id = :testId];
>>>>>>> aff6822d175b84fde3813500d3ada29783087209
    System.assertEquals('Closed', actualStatusOfSelectedPosition.Status__c);
    System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.CONFIRM));
  }

  @isTest
  static void testGetStatusList() {
    PositionByStatusCtrl controllerForSelectOption = new PositionByStatusCtrl();

    Test.startTest();
    List<SelectOption> selOpt = controllerForSelectOption.getStatusList();
    Test.stopTest();

    System.assertEquals('All', selOpt.get(0).getValue());
  }

  @isTest
  static void testFilterPositions() {
    PositionByStatusCtrl controllerForTestFilter = new PositionByStatusCtrl();

    Test.startTest();
    controllerForTestFilter.statusFilter = 'Closed';
    controllerForTestFilter.filterPositions();
    Test.stopTest();

    System.assertEquals(1, controllerForTestFilter.positionList.size());
    System.assertEquals('Closed', controllerForTestFilter.positionList.get(0).Status__c);
  }
<<<<<<< HEAD

=======
  
>>>>>>> aff6822d175b84fde3813500d3ada29783087209
  @isTest
  static void testresultOfFilterComplicatedVersion() {
    PositionByStatusCtrl controllerForTestFilterComplicated = new PositionByStatusCtrl();

    Test.startTest();
    Id testId = controllerForTestFilterComplicated.positionList.get(0).Id;
    controllerForTestFilterComplicated.positionList.get(0).Status__c = 'Closed';
    controllerForTestFilterComplicated.save();
    controllerForTestFilterComplicated.statusFilter = 'Closed';
    controllerForTestFilterComplicated.filterPositions();
<<<<<<< HEAD
    Test.stopTest();

    System.assertEquals(
      2,
      controllerForTestFilterComplicated.positionList.size()
    );
=======
    System.assertEquals(2, controllerForTestFilterComplicated.positionList.size());
>>>>>>> aff6822d175b84fde3813500d3ada29783087209
  }
}
